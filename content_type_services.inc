<?php
//$Id: content_type_services.cart.inc,v 1.1.2.1 2010/06/18 18:20:46 coworks Exp $

/**
 * @file
 * Integration of Services module for content_type
 */
function content_type_services_list($module = NULL) {
  $content_types = node_get_types('types');
  if (!empty($module)) {
    foreach ($content_types as $name => $content_type) {
      if ($content_type->module != $module) {
        unset($content_types[$name]);
      }
    }
  }
  return $content_types;
}

function content_type_services_list_access($uid = NULL) {
  global $user;
  watchdog("mobilesocial debug", "TODO: a hack in content_type_services_list_access should be solved");
  return true;
  return (($user->uid == $uid && user_access('get own user data')) || ($user->uid != $uid && user_access('get any user data')));
}

function content_type_services_fields($content_type = NULL, $module = NULL) {
	if (!empty($content_type)) {
		return content_types($content_type);
	}
	else {
		$content_types = content_types();
		if (!empty($module)) {
			foreach ($content_types as $name => $content_type) {
				if ($content_type['module'] != $module) {
					unset($content_types[$name]);
				}
			}
		}
		return $content_types;
	}
}

function content_type_services_fields_access($uid = NULL) {
  global $user;
  watchdog("mobilesocial debug", "TODO: a hack in content_type_services_list_access should be solved");
  return true;
  return (($user->uid == $uid && user_access('get own user data')) || ($user->uid != $uid && user_access('get any user data')));
}
?>
